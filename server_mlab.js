require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
//const userFile = require('./user_data.json')
const userFile = require('./user_account.json')
const URL_BASE = '/techu/v2/';
const URL_mlab = "https://api.mlab.com/api/1/databases/techu24db/collections/"
const apiKey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;
const cors = require('cors');


app.listen(port,function()
{
  console.log('NodeJS escuchando en el puesto '+port);
}
);

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "*");
 next();  // middleware para decirle que pase por aquí primero
}
app.use(enableCORS);

app.use(cors());
app.options('*', cors());

app.use(body_parser.json());

//Operacion GET con Mlab
  app.get(URL_BASE + 'users_prueba1',
    function(request,response){
      const http_client = request_json.createClient(URL_mlab);
      console.log('Cliente HPPT');
      response.status(200).send({'msj':'Cliente Http'});
    }
  );

  app.get(URL_BASE + 'users_prueba2',
  function(request,response){
    const http_client = request_json.createClient(URL_mlab);
    console.log('Cliente HPPT');
    console.log(apiKey_mlab);
    http_client.get('user?' + apiKey_mlab,
        function(err,res,body){
          response.send(body);
        }
      );
      //response.status(200).send({'msj':'Clienbte Http'});
  }
  );

app.get(URL_BASE + 'users',
    function(req, res) {
      const httpClient = request_json.createClient(URL_mlab);
      console.log("Cliente HTTP mLab creado.");
      const fieldParam = 'f={"_id":0}&';
      httpClient.get('user?' + fieldParam + apiKey_mlab,
        function(err, respuestaMLab, body) {
          console.log('Error: ' + err);
          console.log('Respuesta MLab: ' + respuestaMLab);
          console.log('Body: ' + body);
          var response = {};
          if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
          } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

//Get con Id desde Mlab
app.get(URL_BASE + 'users/:id',
  function(req,res){
    console.log("Get con users/:id");
    console.log(req.params.id);
    let id = req.params.id;
    let queryString = 'q={"userid":'+ id +'}&';
    let queryString_template = `q={"userid":${id}}&`;
    let fieldParam = 'f={"_id":0}&';
    const httpClient = request_json.createClient(URL_mlab);
    httpClient.get('user?' + queryString_template + fieldParam + apiKey_mlab,
      function(error,res_mlab,body){
        let response = {};
        if(error){
          response = {'msg':'Error en la Peticion'};
          res.status(500);
        }else{
          if(body.length>0){
            response = body;
          }else {
            response = {'mgt':'Usuario no encontraso'};
          }
        }
      res.send(response);
  });
  });

//Get Cuentas de Usuario

app.get(URL_BASE + 'users/:id/account',
  function(req,res){
    console.log("Get con users/:id/account");
    console.log(req.params.id);
    let id = req.params.id;
    let queryString = 'q={"userid":'+ id +'}&';
    let fieldParam = 'f={"_id":0}&';
    const httpClient = request_json.createClient(URL_mlab);
    httpClient.get('account?' + queryString + fieldParam + apiKey_mlab,
      function(error,res_mlab,data){
        let response = {};
        response = !error ? data : {'mgt':'Error en la Peticion'};

          if(data.length>0){
            response = data;
          }else {
            response = {'mgt':'Usuario no encontraso'};
          }
      res.send(response);
  });
});

app.get(URL_BASE + 'users/:id/account_mov',
  function(req,res){
    console.log("Get con users/:id/account_mov");
    console.log(req.params.id);
    let id = req.params.id;
    let queryString = 'q={"userId":'+ id +'}&';
    let fieldParam = 'f={"_id":0,"movAccount":1}&';
    const httpClient = request_json.createClient(URL_mlab);
    httpClient.get('account_mov?' + queryString + apiKey_mlab,
      function(error,res_mlab,data){
        let response = {};
        response = !error ? data : {'mgt':'Error en la Peticion'};

          if(data.length>0){
            console.log("respuestaMLab: " + JSON.stringify(res_mlab));
            //response = data[0].account;
            response = data;
          }else {
            response = {'mgt':'Usuario no encontrado'};
          }
      res.send(response);
  });
});

//Post
app.post(URL_BASE + "users",
 function(req, res) {
   console.log(req.body);
  var clienteMlab = request_json.createClient(URL_mlab);
  clienteMlab.get('user?'+ apiKey_mlab ,
  function(error, respuestaMLab , body) {
      newID=body.length+1;
      console.log("newID:" + newID);
      var newUser = {
        "userid" : newID+1,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      clienteMlab.post("user?" + apiKey_mlab, newUser ,
       function(error, respuestaMLab, dataNew) {
        res.send(dataNew);
     });
  });
});

//DELETE user with id
app.delete(URL_BASE + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"userid":' + id + '}&';
    console.log(URL_mlab + 'user?' + queryStringID + apiKey_mlab);
       var httpClient = request_json.createClient(URL_mlab);
       httpClient.get('user?' +  queryStringID + apiKey_mlab,
         function(error, respuestaMLab, body){
           var respuesta = body[0];
           console.log("body delete:"+ JSON.stringify(respuesta));
          httpClient.delete(URL_mlab + "user/" + respuesta._id.$oid +'?'+ apiKey_mlab,
          function(error, respuestaMLab,body){
            res.send(body);
});
});
});

// DELETE con PUT de mLab
app.delete(URL_BASE + "users_mlab/:id",
  function(req, res){
    var id = Number.parseInt(req.params.id);
    var query = 'q={"userid":' + id + '}&';
    // Interpolación de expresiones
    var query2 = `q={"userid":${id} }&`;
    var query3 = "q=" + JSON.stringify({"id": id})
 + '&';
    var httpClient = request_json.createClient(URL_mlab);
    httpClient.put("user?" + query + apiKey_mlab, [{}],
        function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error borrando usuario."
          }
        res.send(response);
      }
    );
  });

//Method POST login
  app.post(URL_BASE + "login",
    function (req, res){
      console.log("POST /colapi/v3/login");
      let email = req.body.email;
      let pass = req.body.password;
      let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
      let limFilter = 'l=1&';
      let clienteMlab = request_json.createClient(URL_mlab);
      clienteMlab.get('user?'+ queryString + limFilter + apiKey_mlab,
      function(error, respuestaMLab, body) {
          if(!error) {
            if (body.length == 1) { // Existe un usuario que cumple 'queryString'
              let login = '{"$set":{"logged":true}}';
              clienteMlab.put('user?q={"userid": ' + body[0].userid + '}&' + apiKey_mlab, JSON.parse(login),
              //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),

              function(errPut, resPut, bodyPut) {
                console.log(body[0]);
                res.send({'msg':'Login correcto',
                            'email':body[0].email,
                            'userid':body[0].userid,
                            'first_name':body[0].first_name,
                            'admin':body[0].admin}
                          );
              });
          }
          else {
            res.send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("POST /techu24db/v2/logout");
    let email = req.body.email;
    let queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    let  clienteMlab = request_json.createClient(URL_mlab);
    clienteMlab.get('user?'+ queryString + apiKey_mlab,
    function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"userid": ' + respuesta.userid + '}&' + apiKey_mlab, JSON.parse(logout),
            function(errPut, resPut, bodyPut) {
               res.send({'msg':'Logout correcto', 'user':respuesta.email});
             });
           } else {
               res.status(404).send({"msg":"Logout failed!"});
           }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
});
