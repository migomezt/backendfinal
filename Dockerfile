# Imagen docker base inicial
FROM node:latest

# Crea directorio de trabajo del contenedor Docker
WORKDIR /docker-apitech-mgt

# Copiar archivos del proyecto en el directorio de trabajo Docker
ADD . /docker-apitech-mgt

# Instalar dependencias produccion del proyecto
#RUN npm install --only=production


# Puerto donde exponemos contenedor (mismo que usamos en nuestra API)
EXPOSE 3000

# Comando para lanzar la app
CMD ["npm","run", "prod"]
